package space.unkovsky.locationtracker

import android.app.Application
import io.reactivex.schedulers.Schedulers
import space.unkovsky.locationtracker.data.local.dao.AppStateDao
import space.unkovsky.locationtracker.di.AppComponent
import space.unkovsky.locationtracker.di.DaggerAppComponent
import space.unkovsky.locationtracker.di.modules.AppModule
import space.unkovsky.locationtracker.di.modules.LocationRepositoryModule
import space.unkovsky.locationtracker.di.modules.RoomModule

class App : Application() {
    lateinit var appStateDao: AppStateDao

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }

    val component: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .roomModule(RoomModule())
                .locationRepositoryModule(LocationRepositoryModule())
                .build()
    }

    val appState by lazy {
        appStateDao.getState().observeOn(Schedulers.io()).subscribeOn(Schedulers.io())
    }
}
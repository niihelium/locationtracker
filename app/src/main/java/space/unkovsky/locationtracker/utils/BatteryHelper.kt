package space.unkovsky.locationtracker.utils

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager

abstract class BatteryHelper{
    companion object {
        //This should be called explicitly
        fun getBatteryPercentage(context: Context): Int {
            val iFilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
            val batteryStatus = context.registerReceiver(null, iFilter)

            return getCurrentBatteryLevel(batteryStatus)
        }
        private fun getCurrentBatteryLevel(intent: Intent): Int {
            val level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
            val scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)

            val batteryPct = level / scale.toFloat()
            return (batteryPct * 100).toInt()
        }
    }
}
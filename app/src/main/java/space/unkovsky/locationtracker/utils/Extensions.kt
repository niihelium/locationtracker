package space.unkovsky.locationtracker.utils

import android.app.Service
import space.unkovsky.locationtracker.App

val Service.app: App
    get() = application as App
package space.unkovsky.locationtracker.utils

const val UPDATE_INTERVAL_IN_MILLISECONDS = 10000L
const val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS /2
const val LOCATION_PERMISSION_REQUEST = 0
const val LOCATION_ACTION = "location_tracker_location_action"
const val LOCATION_MESSAGE = "location_tracker_location_message"
const val BATTERY_MESSAGE = "location_tracker_battery_message"

const val APP_STATE_DB_NAME = "app_state"

const val SYNC_INTERVAL = 5000L


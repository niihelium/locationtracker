package space.unkovsky.locationtracker.utils

import android.os.Build


fun isM() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
fun isO() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O

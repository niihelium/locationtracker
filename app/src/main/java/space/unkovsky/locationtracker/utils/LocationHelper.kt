package space.unkovsky.locationtracker.utils

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.google.android.gms.location.LocationRequest
import space.unkovsky.locationtracker.LocationService

abstract class LocationHelper {
    companion object {
        const val ACTION_PROCESS_LOCATION = "process_activity"


        fun createIntent(context: Context): PendingIntent {
            val intent = Intent(context, LocationService::class.java)
            intent.action = ACTION_PROCESS_LOCATION
            return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        fun createLocationRequest(intervalArg: Long) = LocationRequest.create().apply {
            interval = intervalArg
            fastestInterval = interval / 2
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            maxWaitTime = interval * 3
        }

        fun formatLocationResult(latitude: Double, longitude: Double) = "Lat:$latitude Long:$longitude"
    }
}
package space.unkovsky.locationtracker.utils

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.google.android.gms.location.ActivityRecognition
import com.google.android.gms.location.ActivityTransition
import com.google.android.gms.location.ActivityTransitionRequest
import com.google.android.gms.location.DetectedActivity
import space.unkovsky.locationtracker.BroadcastReceivers.TransitionIntentService

abstract class ActivityHelper {
    companion object {
        public const val ACTION_PROCESS_ACTIVITY = "process_activity"

        fun createIntent(context: Context): PendingIntent {
            val intent = Intent(context, TransitionIntentService::class.java)
            intent.action = ACTION_PROCESS_ACTIVITY
            //intent.action = action
            //intent.putExtra("extra", extra)
            return PendingIntent.getService(context, 213, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        fun createActivityRequest() =
                ActivityTransitionRequest(
                        arrayListOf(
                                createTransition(DetectedActivity.IN_VEHICLE, ActivityTransition.ACTIVITY_TRANSITION_EXIT),
                                createTransition(DetectedActivity.IN_VEHICLE, ActivityTransition.ACTIVITY_TRANSITION_ENTER),
                                createTransition(DetectedActivity.ON_BICYCLE, ActivityTransition.ACTIVITY_TRANSITION_EXIT),
                                createTransition(DetectedActivity.ON_BICYCLE, ActivityTransition.ACTIVITY_TRANSITION_ENTER),
                                createTransition(DetectedActivity.RUNNING, ActivityTransition.ACTIVITY_TRANSITION_EXIT),
                                createTransition(DetectedActivity.RUNNING, ActivityTransition.ACTIVITY_TRANSITION_ENTER),
                                createTransition(DetectedActivity.WALKING, ActivityTransition.ACTIVITY_TRANSITION_EXIT),
                                createTransition(DetectedActivity.WALKING, ActivityTransition.ACTIVITY_TRANSITION_ENTER),
                                createTransition(DetectedActivity.STILL, ActivityTransition.ACTIVITY_TRANSITION_EXIT),
                                createTransition(DetectedActivity.STILL, ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                        )
                )


        fun createTransition(activityType: Int, transaction: Int) =
                ActivityTransition.Builder()
                        .setActivityType(activityType)
                        .setActivityTransition(transaction)
                        .build()

        fun startActivityUpdates(context: Context) {
            val intent = createIntent(context)
            ActivityRecognition.getClient(context)
                    .requestActivityTransitionUpdates(
                            createActivityRequest(),
                            intent)
                    .addOnSuccessListener {
                        //                    Toast.makeText(applicationContext,
//                            "Successfully requested activity updates",
//                            Toast.LENGTH_SHORT)
//                            .show()
                    }
                    .addOnFailureListener {
                        //                    Toast.makeText(applicationContext,
//                            "Requesting activity updates failed to start",
//                            Toast.LENGTH_SHORT)
//                            .show()
                    }
        }

        private fun stopActivityUpdates(context: Context) {
            ActivityRecognition.getClient(context).removeActivityUpdates(createIntent(context))
        }

    }

}
package space.unkovsky.locationtracker.di.modules

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import space.unkovsky.locationtracker.data.LocationRepository
import space.unkovsky.locationtracker.data.local.dao.AppStateDao
import space.unkovsky.locationtracker.data.local.dao.LocationEventDao
import space.unkovsky.locationtracker.data.remote.LocationApi
import javax.inject.Singleton

@Module
class LocationRepositoryModule {

    @Provides
    @Singleton
    fun provideLocationApi() =
            LocationApi()

    @Provides
    @Singleton
    fun provideLocationRepository(api: LocationApi,
                                  locationEventDao: LocationEventDao,
                                  appStateDao: AppStateDao) =
            LocationRepository(api, locationEventDao, appStateDao)
}
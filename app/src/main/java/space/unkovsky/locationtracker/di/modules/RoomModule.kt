package space.unkovsky.locationtracker.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import space.unkovsky.locationtracker.data.local.AppDatabase
import javax.inject.Singleton

@Module
class RoomModule {
    @Provides
    @Singleton
    fun provideAppDatabase(context: Context) = Room.databaseBuilder(context, AppDatabase::class.java, "app_database").build()

    @Provides
    @Singleton
    fun provideLocationEventDao(database: AppDatabase) = database.locationEventDao()

    @Provides
    @Singleton
    fun provideAppStateDao(database: AppDatabase) = database.appStateDao()
}
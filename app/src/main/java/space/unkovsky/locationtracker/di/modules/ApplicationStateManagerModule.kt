package space.unkovsky.locationtracker.di.modules

import dagger.Module
import dagger.Provides
import space.unkovsky.locationtracker.AppStateManager
import space.unkovsky.locationtracker.data.local.dao.AppStateDao
import javax.inject.Singleton

@Module
class ApplicationStateManagerModule {
    @Provides
    @Singleton
    fun provideAppStateManager(appStateDao: AppStateDao) = AppStateManager(appStateDao)
}
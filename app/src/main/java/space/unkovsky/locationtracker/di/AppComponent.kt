package space.unkovsky.locationtracker.di

import dagger.Component
import space.unkovsky.locationtracker.App
import space.unkovsky.locationtracker.BroadcastReceivers.BatteryReceiver
import space.unkovsky.locationtracker.BroadcastReceivers.TransitionIntentService
import space.unkovsky.locationtracker.LocationService
import space.unkovsky.locationtracker.MainActivity
import space.unkovsky.locationtracker.di.modules.AppModule
import space.unkovsky.locationtracker.di.modules.LocationRepositoryModule
import space.unkovsky.locationtracker.di.modules.RoomModule
import javax.inject.Singleton

@Component(modules = arrayOf(
        AppModule::class,
        RoomModule::class,
        LocationRepositoryModule::class))
@Singleton
interface AppComponent {
    fun inject(service: LocationService)
    fun inject(app: App)
    fun inject(activity: MainActivity)
    //fun inject(activityReceiver: ActivityReceiver)
    fun inject(transitionIntentService: TransitionIntentService)
    fun inject(batteryReceiver: BatteryReceiver)
}
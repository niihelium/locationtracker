package space.unkovsky.locationtracker.data.local.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import space.unkovsky.locationtracker.data.local.entities.AppState
import space.unkovsky.locationtracker.utils.APP_STATE_DB_NAME

@Dao
interface AppStateDao {

    @Query("SELECT * from $APP_STATE_DB_NAME")
    fun getAll(): List<AppState>

    @Query("SELECT * FROM app_state WHERE stateId = 1")
    fun getState(): Flowable<List<AppState>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(appState: AppState)

    @Query("UPDATE app_state SET lastSync=:lastSync WHERE stateId = 1")
    fun updateLastSync(lastSync: Long)

    @Query("UPDATE app_state SET remoteSyncInterval=:interval WHERE stateId = 1")
    fun updateSyncInterval(interval: Long)

    @Query("UPDATE app_state SET locationUpdateInterval=:interval WHERE stateId = 1")
    fun updateLocationInterval(interval: Long)

}
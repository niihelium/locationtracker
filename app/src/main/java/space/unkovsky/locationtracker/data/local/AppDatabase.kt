package space.unkovsky.locationtracker.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import space.unkovsky.locationtracker.data.local.dao.AppStateDao
import space.unkovsky.locationtracker.data.local.dao.LocationEventDao
import space.unkovsky.locationtracker.data.local.entities.AppState
import space.unkovsky.locationtracker.data.local.entities.LocationEvent

@Database(entities = [(AppState::class), (LocationEvent::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun locationEventDao(): LocationEventDao
    abstract fun appStateDao(): AppStateDao
}
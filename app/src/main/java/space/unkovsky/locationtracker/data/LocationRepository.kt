package space.unkovsky.locationtracker.data

import android.util.Log
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import space.unkovsky.locationtracker.data.local.dao.AppStateDao
import space.unkovsky.locationtracker.data.local.dao.LocationEventDao
import space.unkovsky.locationtracker.data.local.entities.LocationEvent
import space.unkovsky.locationtracker.data.remote.LocationApi

class LocationRepository(val locationApi: LocationApi, val locationEventDao: LocationEventDao, val appStateDao: AppStateDao) {
    fun storeLocationInDb(locations: List<LocationEvent>) {

        Observable.fromCallable { locationEventDao.insertAll(locations) }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe {
                }
    }

    fun syncFrom(time: Long) {
        locationEventDao.getAfterTimestamp(time)
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe({ list ->
                    saveToFile(list)
                }, { e ->
                    Log.e("Location repo", e.message)
                })
    }

    fun saveToFile(locations: List<LocationEvent>) {
        locationApi.fakeUploadEvents(locations)
//                .doOnComplete {
//                    appStateDao.updateSyncInterval(System.currentTimeMillis())
//                }
//                .doOnError {
//                    Log.e("saveToFile", "Error", it)
//                }
    }
}
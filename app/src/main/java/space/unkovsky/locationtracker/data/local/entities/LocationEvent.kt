package space.unkovsky.locationtracker.data.local.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.location.Location

@Entity(tableName = "location_events")
data class LocationEvent(
        @PrimaryKey(autoGenerate = true)
        val id: Long = 0,
        val latitude: Double,
        val longitude: Double,
        val batteryPercentage: Int,
        val timestamp: Long
)
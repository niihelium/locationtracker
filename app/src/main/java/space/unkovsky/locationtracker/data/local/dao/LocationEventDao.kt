package space.unkovsky.locationtracker.data.local.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import io.reactivex.Single
import space.unkovsky.locationtracker.data.local.entities.LocationEvent

@Dao
interface LocationEventDao {

    @Query("SELECT * from location_events")
    fun getAll(): Flowable<List<LocationEvent>>

    @Query("SELECT * from location_events where timestamp > :timestamp")
    fun getAfterTimestamp(timestamp: Long): Single<List<LocationEvent>>

    @Insert(onConflict = REPLACE)
    fun insert(LocationEvent: LocationEvent): Long

    @Insert(onConflict = REPLACE)
    fun insertAll(LocationEvent: List<LocationEvent>): List<Long>
}
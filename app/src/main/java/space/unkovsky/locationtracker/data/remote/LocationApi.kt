package space.unkovsky.locationtracker.data.remote

import android.os.Environment
import android.util.Log
import space.unkovsky.locationtracker.data.local.entities.LocationEvent
import java.io.File
import java.io.FileOutputStream

class LocationApi {

    val filename = "locations.csv"

//    @GET("6de6abfedb24f889e0b5f675edc50deb?fmt=raw&sole")
//    fun getUsers(): Observable<List<LocationEvent>>

    fun fakeUploadEvents(events: List<LocationEvent>) =
            writeLocationsToFile(events, openFile(filename, getPublicStorageDir("locationData")))


    fun isExternalStorageWritable(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }


    fun getPublicStorageDir(dirName: String): File? {
        // Get the directory for the user's public pictures directory.
        val root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        val dir = File(root.absolutePath, "/$dirName")
        //dir.mkdirs()
        if (!dir.mkdirs()) {
            Log.e("Location API", "Directory not created")
        }
        return dir
    }


    private fun openFile(filename: String, filesDir: File?): FileOutputStream {
        val fOut = FileOutputStream(File(filesDir, filename), true)
        return fOut
    }


    private fun writeLocationsToFile(events: List<LocationEvent>, file: FileOutputStream) {
        for (event in events) {
            file.write("${event.latitude}, ${event.latitude}, ${event.timestamp}, ${event.batteryPercentage}\n".toByteArray())
        }
        file.flush()
        file.close()
    }


}
package space.unkovsky.locationtracker.data.local.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import space.unkovsky.locationtracker.utils.APP_STATE_DB_NAME

@Entity(tableName = APP_STATE_DB_NAME)
data class AppState(
        @PrimaryKey(autoGenerate = true)
        val stateId: Long = 0,
        val lastSync: Long,
        val locationUpdateInterval: Long,
        val remoteSyncInterval: Long
)
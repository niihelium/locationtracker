package space.unkovsky.locationtracker.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.location_item.view.*
import space.unkovsky.locationtracker.R
import space.unkovsky.locationtracker.data.local.entities.LocationEvent
import space.unkovsky.locationtracker.utils.LocationHelper

class LocationsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var data: MutableList<LocationEvent> = mutableListOf()
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.location_item, parent, false)
        return LocationsViewHolder(view)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        (holder as LocationsViewHolder).bindItem(data[position])
    }

    fun addItems(items: MutableList<LocationEvent>){
        data.addAll(items)
        notifyDataSetChanged()
    }

    class LocationsViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bindItem(item: LocationEvent) {
            with(item) {
                //itemView.start_time.text = item.startTime.toString()
                itemView.location.text = LocationHelper.formatLocationResult(item.longitude, item.latitude)
                itemView.date.text = item.timestamp.toString()
                itemView.batteryState.text = item.batteryPercentage.toString()
            }
        }
    }
}
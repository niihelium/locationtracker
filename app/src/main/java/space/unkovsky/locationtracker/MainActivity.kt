package space.unkovsky.locationtracker

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import space.unkovsky.locationtracker.data.LocationRepository
import space.unkovsky.locationtracker.data.local.entities.LocationEvent
import space.unkovsky.locationtracker.ui.LocationsAdapter
import space.unkovsky.locationtracker.utils.*
import javax.inject.Inject


class MainActivity : AppCompatActivity() {
    private val locationReceiver = LocationReceiver()

    @Inject
    lateinit var repo: LocationRepository

    private val runtimePermissions = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as App).component.inject(this)
        startTrackingButton.setOnClickListener {
            startTrackingLocation()
            startTrackingActivity()
        }
        val adapter = LocationsAdapter()
        locationsList.adapter = adapter
        locationsList.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)

        repo.locationEventDao.getAll()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { t: List<LocationEvent> ->
                            adapter.addItems(t.toMutableList())
                        },
                        { t: Throwable -> Log.e("MainActivity", t.toString()) })

    }

    private fun startTrackingActivity() {
        ActivityHelper.startActivityUpdates(this.applicationContext)
    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this).registerReceiver(locationReceiver, IntentFilter(LOCATION_ACTION))
    }

    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(locationReceiver)
    }

    private fun startTrackingLocation() {
        if (isM())
            checkPermissions()
        else
            startLocationService()
    }

    private fun startLocationService() {
        val serviceIntent = Intent(this, LocationService::class.java)
        //Since android O system can kill our service, so we should make it foreground
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(serviceIntent)
        } else {
            startService(serviceIntent)

        }
    }

    fun hasPermissions(context: Context, permissions: Array<String>): Boolean {
        for (permission in permissions) {
            if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    private fun checkPermissions() {
        if (!hasPermissions(applicationContext, runtimePermissions)) {
            ActivityCompat.requestPermissions(this,
                    runtimePermissions,
                    LOCATION_PERMISSION_REQUEST)

        } else {
            startLocationService()
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startLocationService()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
            else -> {
            }
        }
    }


    private inner class LocationReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent?) {
            if (null != intent && intent.action == LOCATION_ACTION) {

                val locationData = intent.getStringExtra(LOCATION_MESSAGE)
                val batteryData = intent.getIntExtra(BATTERY_MESSAGE, 0)

                locationTextView.text = locationData
                batteryTextView.text = "$batteryData%"
            }
        }
    }
}

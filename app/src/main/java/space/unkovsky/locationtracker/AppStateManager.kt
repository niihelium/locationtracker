package space.unkovsky.locationtracker

import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import space.unkovsky.locationtracker.BroadcastReceivers.BatteryReceiver
import space.unkovsky.locationtracker.BroadcastReceivers.TransitionIntentService
import space.unkovsky.locationtracker.data.local.dao.AppStateDao
import space.unkovsky.locationtracker.data.local.entities.AppState
import javax.inject.Inject

class AppStateManager @Inject constructor(private val appStateDao: AppStateDao) {
    companion object {
        val defaultLocationUpdateInterval = 10_000L
        val defaultSyncInterval = 10_000L
    }


    private var batteryState: Double = BatteryReceiver.ACCURACY_MED
    private var activityState: Double = TransitionIntentService.ACCURACY_NORMAL


    private val appState = appStateDao.getState()


    fun updateLastSync() {
        Observable.fromCallable {
            appStateDao.updateLastSync(System.currentTimeMillis())
        }
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe {
                    Log.i("MainActivity", "inserted")
                }

    }

    fun changeActivityState(activityState: Double) {
        updateAccuracy(activityState = activityState)
    }

    fun changeBatteryState(batteryState: Double) {
        updateAccuracy(batteryState = batteryState)
    }

    fun getApplicationState() = appState.observeOn(AndroidSchedulers.mainThread())

    private fun updateAccuracy(batteryState: Double = this.batteryState,
                               activityState: Double = this.activityState) {
        doAsync {
            appStateDao.updateLocationInterval(
                    (defaultLocationUpdateInterval * (batteryState + activityState)).toLong())
        }
    }

    public fun populateAppState() {
        doAsync {
            appStateDao.insert(
                    AppState(1,
                            System.currentTimeMillis(),
                            defaultLocationUpdateInterval,
                            defaultSyncInterval))
        }
    }
}
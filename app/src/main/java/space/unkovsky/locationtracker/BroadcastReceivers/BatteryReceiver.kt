package space.unkovsky.locationtracker.BroadcastReceivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.BatteryManager
import android.os.BatteryManager.BATTERY_PLUGGED_AC
import android.os.BatteryManager.BATTERY_PLUGGED_USB
import space.unkovsky.locationtracker.App
import space.unkovsky.locationtracker.AppStateManager
import javax.inject.Inject


// Intent.ACTION_BATTERY_CHANGED should be registered only explicitly
class BatteryReceiver : BroadcastReceiver() {
    companion object {
        val ACCURACY_MAX_AC = .1
        val ACCURACY_MAX_USB = .5
        val ACCURACY_MED = .8
        val ACCURACY_MIN = 1.0
    }


    @Inject
    lateinit var locationManager: AppStateManager

    /**
     * When we receive new battery event we can change global app state according to this state
     * */
    override fun onReceive(context: Context, intent: Intent) {
        (context.applicationContext as App).component.inject(this)
        handleAction(intent)
    }

    fun handleAction(intent: Intent) {
        when (intent.action) {
            Intent.ACTION_BATTERY_LOW -> {
                setAccuracy(ACCURACY_MIN)
            }
             Intent.ACTION_POWER_CONNECTED -> {
                when (getChargePlug(intent)) {
                    BATTERY_PLUGGED_AC -> setAccuracy(ACCURACY_MAX_AC)
                    BATTERY_PLUGGED_USB -> setAccuracy(ACCURACY_MAX_USB)
                }
            }
            Intent.ACTION_BATTERY_OKAY, Intent.ACTION_POWER_DISCONNECTED -> {
                setAccuracy(ACCURACY_MED)
            }
        }
    }

    fun getChargePlug(intent: Intent) = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1)


    fun setAccuracy(accuracy: Double) {
        locationManager.changeBatteryState(accuracy)
    }
}
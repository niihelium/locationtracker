package space.unkovsky.locationtracker.BroadcastReceivers

import android.app.IntentService
import android.content.Intent
import android.util.Log
import com.google.android.gms.location.*
import space.unkovsky.locationtracker.App
import space.unkovsky.locationtracker.AppStateManager
import javax.inject.Inject


class TransitionIntentService : IntentService("TransitionIntentService") {
    companion object {
        val ACCURACY_FAST_MOVING = .1
        val ACCURACY_NORMAL = 0.5
        val ACCURACY_STATIONARY = 1.0
    }

    private val TAG = TransitionIntentService::class.java.simpleName

    @Inject
    lateinit var locationManager: AppStateManager

    override fun onHandleIntent(intent: Intent?) {
        (applicationContext as App).component.inject(this)
        intent?.let {
            handleActivityIntent(it)
        }
    }

    private fun handleActivityIntent(intent: Intent) {
        if (ActivityTransitionResult.hasResult(intent)) {
            val result = ActivityTransitionResult.extractResult(intent)
            result?.let {
                handleActivityTransition(it.transitionEvents)
            }

        } else if (ActivityRecognitionResult.hasResult(intent)) {
            val result = ActivityRecognitionResult.extractResult(intent)
            result?.let {
                handleActivityRecognition(it.mostProbableActivity)
            }
        }
    }

    private fun handleActivityTransition(activityTransitionResults: List<ActivityTransitionEvent>) {
        val result = activityTransitionResults.last()
        if (result.transitionType == ActivityTransition.ACTIVITY_TRANSITION_ENTER) {
            processActivityType(result.activityType)
        }
    }

    private fun handleActivityRecognition(detectedActivity: DetectedActivity) {
        processActivityType(detectedActivity.type)
    }

    private fun processActivityType(activityType: Int) {
        when (activityType) {
            DetectedActivity.IN_VEHICLE, DetectedActivity.ON_BICYCLE -> {
                Log.e("ActivityRecogition", "In Vehicle: ")
                locationManager.changeActivityState(ACCURACY_FAST_MOVING)
            }
            DetectedActivity.ON_FOOT, DetectedActivity.RUNNING, DetectedActivity.WALKING -> {
                Log.e("ActivityRecogition", "On Foot: ")
                locationManager.changeActivityState(ACCURACY_NORMAL)
            }
            DetectedActivity.UNKNOWN, DetectedActivity.TILTING, DetectedActivity.STILL -> {
                Log.e("ActivityRecogition", "Unknown: ")
                locationManager.changeActivityState(ACCURACY_STATIONARY)
            }
        }
    }
}
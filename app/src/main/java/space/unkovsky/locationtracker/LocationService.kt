package space.unkovsky.locationtracker

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.provider.SyncStateContract
import android.support.v4.app.NotificationCompat
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.google.android.gms.location.LocationAvailability
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import space.unkovsky.locationtracker.data.LocationRepository
import space.unkovsky.locationtracker.data.local.entities.AppState
import space.unkovsky.locationtracker.data.local.entities.LocationEvent
import space.unkovsky.locationtracker.utils.*
import javax.inject.Inject


class LocationService : Service() {
    private val TAG = "LocationService"

    @Inject
    lateinit var locationRepository: LocationRepository
    @Inject
    lateinit var appStateManager: AppStateManager

    lateinit var appState: AppState

    private var isStarted = false
    private var updateInterval = UPDATE_INTERVAL_IN_MILLISECONDS

    override fun onCreate() {
        super.onCreate()
        buildNotification()
    }


    fun buildNotification() {
        if (Build.VERSION.SDK_INT >= 26) {

            val CHANNEL_ID = "my_channel_01"
            val channel = NotificationChannel(CHANNEL_ID,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT)

            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(channel)

            val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("")
                    .setContentText("").build()

            startForeground(1, notification)
        }
    }

    override fun onBind(intent: Intent?) = null

    private fun initializeDagger() = app.component.inject(this)

    @SuppressLint("CheckResult")
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand")
        initializeDagger()
        appStateManager.getApplicationState()
                .subscribe {
                    if (it.isNotEmpty()) {
                        appState = it[0]

                        if (!isStarted) {
                            startLocationUpdates()
                            isStarted = true
                        }
                        if (updateInterval != appState.locationUpdateInterval) {
                            updateInterval = appState.locationUpdateInterval
                            changeAccuracy() //yep, same code
                            isStarted = true
                        }
                    } else {
                        appStateManager.populateAppState()
                    }

                }

        return START_STICKY
    }

    private fun changeAccuracy() {
        try {
            LocationServices
                    .getFusedLocationProviderClient(this)
                    .requestLocationUpdates(
                            LocationHelper.createLocationRequest(appState.locationUpdateInterval),
                            locationCallback,
                            null)
        } catch (e: SecurityException) {
            Log.e("LocationService", e.toString())
        }
    }

    private fun startLocationUpdates() {
        try {
            LocationServices
                    .getFusedLocationProviderClient(this)
                    .requestLocationUpdates(
                            LocationHelper.createLocationRequest(appState.locationUpdateInterval),
                            locationCallback,
                            null)
        } catch (e: SecurityException) {
            Log.e("LocationService", e.toString())
        }
    }

    private fun stopLocationUpdates() {
        LocationServices.getFusedLocationProviderClient(this).removeLocationUpdates(locationCallback)
    }


    override fun onDestroy() {
        stopLocationUpdates()
        super.onDestroy()
    }


    //==============INTERCONNECTION===========
    private fun sendLocationBroadcast(sbLocationData: String, batteryPercentage: Int) {

        val locationIntent = Intent()
        locationIntent.apply {
            action = LOCATION_ACTION
            putExtra(LOCATION_MESSAGE, sbLocationData)
            putExtra(BATTERY_MESSAGE, batteryPercentage)
        }


        LocalBroadcastManager.getInstance(this).sendBroadcast(locationIntent)
    }

    //===============CALLBACKS==============

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationsResult: LocationResult?) {
            locationsResult?.locations?.let {
                val locationEvents = mutableListOf<LocationEvent>()
                val batteryPercentage = BatteryHelper.getBatteryPercentage(app)
                for (location in locationsResult.locations)
                    locationEvents.add(LocationEvent(
                            longitude = location.longitude,
                            latitude = location.latitude,
                            batteryPercentage = batteryPercentage,
                            timestamp = location.time))
                locationRepository.storeLocationInDb(locationEvents)
                //if (isSyncNecessary())
                if (isSyncNecessary()) {
                    locationRepository.syncFrom(appState.lastSync)
                    appStateManager.updateLastSync()
                }
                sendLocationBroadcast("Lat:${locationsResult.lastLocation.latitude} Long:${locationsResult.lastLocation.longitude}", batteryPercentage)
            }
        }

        override fun onLocationAvailability(p0: LocationAvailability?) {
            super.onLocationAvailability(p0)
        }
    }

    private fun isSyncNecessary() = System.currentTimeMillis() - appState.lastSync > AppStateManager.defaultSyncInterval
}